/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sasina.midterm1;

/**
 *
 * @author admin
 */
public class TestStationery {
    public static void main(String[] args) {
        Stationery pen1 = new Stationery("pen1","Blue",1);
        pen1.Write("Pen");
        
        System.out.println("------------------");
        
        Stationery HLpen1 = new Stationery("higlightpen1","Yellow",2);
        HLpen1.Write("HighlightPen");
        
        System.out.println("------------------");
        
        Stationery pen2 = new Stationery("pen2","Rainbow",1);
        pen2.Write("Pen");
        
        System.out.println("------------------");
        
        Stationery pencil1 = new Stationery("pencil1","Black",1);
        pencil1.Write("Pencil");
        
        System.out.println("------------------");
        
        pen2.Delete("Pen");
        
        System.out.println("------------------");
        
        Stationery eraser1 = new Stationery("eraser1","White",6);
        eraser1.Write("Eraser");
        eraser1.Delete("Eraser");
        
        System.out.println("------------------");
        
        Stationery CRpen1 = new Stationery("correctionpen1","Blue",1);
        CRpen1.Write("CorrectionPen");
        CRpen1.Delete("CorrectionPen");
        
        System.out.println("------------------");
        
        Stationery HLpen2 = new Stationery("higlightpen2","Orange",1);
        HLpen2.Write("HighlightPen");
        HLpen2.Delete("HighlightPen");
        
        System.out.println("------------------");
        
        Stationery HLpen3 = new Stationery("higlightpen3","Blue",2);
        HLpen3.Write("HighlightPen");
        HLpen3.Delete("HighlightPen");
        
        
    }
    
}
