/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sasina.midterm1;

/**
 *
 * @author admin
 */
public class Stationery {
    private String name;
    private String color;
    private int numberOfSide;
    
    public Stationery(String name, String color, int numberOfSide){
        this.name = name;
        this.color = color;
        this.numberOfSide = numberOfSide;
        System.out.println("Stationery Created");
        System.out.println("Stationery name : " + this.name + " color : " + 
                this.color + " numberOfSide : " + this.numberOfSide);
        
    }
    public void Write(String type) {
        switch (type){
            case "Pen" :
            case "Pencil" :
                System.out.println(this.name + " Write word");
                break;
            case "HighlightPen" :
                System.out.println(this.name + " Highlight word");
                break;
            default :
                System.out.println(this.name + " Can't Write Word");
            
        }
    }
    
    public void Delete(String type){
        switch (type) {
            case "Eraser" :
            case "CorrectionPen" :
                System.out.println(this.name + " Delete Word");
                break;
            default :
                System.out.println(this.name + " Can't Delete Word");
        }
    }
    
}
